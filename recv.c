#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "link_emulator/lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

typedef struct {
  int checksum;	// For corrupt receive
  int index;	// For lost/corrupt receive
  int len;		// For reading
  char mypayload[1388];
} mymsg;

int main(int argc,char** argv){

  msg r,ack;
  msg t;
  init(HOST,PORT);
  mymsg q;

  // ---------------- Get name ----------------
  recv_message(&r);

  char* file_name = (char*) malloc(15*sizeof(char));
  strcpy(file_name, "recv_");
  strcat(file_name, r.payload);
  int fout = open(file_name, O_CREAT | O_TRUNC| O_WRONLY, 0644);

  send_message(&ack);
  // ------------------------------------------

  // --------------- Get length ---------------
  int cadre = 0;

  while (cadre == 0) {
    recv_message(&t);
    cadre = (atoi(t.payload) / 1388) + 1;
  }

  send_message(&ack);
  // ------------------------------------------

  mymsg *mymsg_vec = malloc(cadre * 1400);

  // Counts number of messages received (for breaak)
  int counter = 1;

  while (1) {
  	// Receive a message
    recv_message(&r);
    memcpy(&q, r.payload, 1400);
    
    // Send ack with message index
    sprintf(ack.payload, "%d", q.index);

    // Check for corrupt message
    int j, check;
    check = q.index ^ q.len;
    for (j = 0; j < 1388; j++) {
    	check ^= q.mypayload[j];
    }

    // If not corrupt then proceed
    // Else do nothing (no ack)
    if (check == q.checksum) {
	    mymsg_vec[q.index] = q;

	    // End clause
	    if (counter >= cadre) {
	      int i;

	      for (i = 0; i < cadre ; i++) {
	        r.len = write(fout, mymsg_vec[i].mypayload, mymsg_vec[i].len);
	      }

	      // Send ack with index for received message
	      send_message(&ack);
	      break;
	    }

	    // Good message
	    counter++;
	    send_message(&ack);
	}
  }

  // Free memory
  free(file_name);
  free(mymsg_vec);
  close(fout);

  return 0;
}