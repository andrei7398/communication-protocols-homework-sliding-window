#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "link_emulator/lib.h"

#define HOST "127.0.0.1"
#define PORT 10000

typedef struct {
  int checksum; // For corrupt receive
  int index;	// For lost/corrupt receive
  int len;		// For reading
  char mypayload[1388];
} mymsg;

int main(int argc,char** argv){
  init(HOST,PORT);
  msg t,r;
  mymsg q;

  int delay = 2 * atoi(argv[3]);

  // ------- SENDING FILE NAME -------
  memcpy(t.payload, argv[1], strlen(argv[1]));
  t.len = strlen(t.payload) + 1;
  send_message(&t);
  while(recv_message_timeout(&r, delay) < 0) {
  	send_message(&t);
  }
  // ---------------------------------

  FILE* fid = fopen(argv[1], "rb");

  // ------- SENDING FILE SIZE -------
  fseek(fid, 0L, SEEK_END);
  int file_size = ftell(fid);
  rewind(fid);

  sprintf(t.payload,"%u",file_size);
  t.len = strlen(t.payload) + 1;
  send_message(&t);
  while (recv_message_timeout(&r, delay) < 0) {
      send_message(&t);
  }
  // ---------------------------------

  // Always sizeof(mymsg)
  t.len = 1400;

  fclose(fid);

  int input_file = open(argv[1], O_RDONLY);

  int cadre = (file_size / 1388) + 1;

  int window_size = (atoi(argv[2]) * atoi(argv[3]) * 1000) / (1400 * 8);

  // Special case
  if (window_size > cadre)
    window_size = cadre;

  // Keeping all messages sent for resend if necessary
  mymsg *mymsg_vec = malloc(cadre * 1400);

  // Keeping evidence of unreceived messages
  int *sended = (int*)calloc(cadre, sizeof(int));

  // Loading slinding window
  int i;
  for (i = 0; i < window_size; i++) {

    q.len = read(input_file, q.mypayload, 1388);
    q.index = i;

    q.checksum = q.index;
    q.checksum ^= q.len;
    int j;
    for (j = 0; j < 1388; j++)
    	q.checksum ^= q.mypayload[j];

    // Adding message in vector
    mymsg_vec[i] = q;

    memcpy(t.payload, &q, 1400);
    send_message(&t);
  }

  // Keepng sliding window full
  for (i = window_size; i < cadre; i++) {
    // Checking  message by index
    if (recv_message_timeout(&r, delay) > 0) {
      sended[atoi(r.payload)] = 1;
    }

    q.len = read(input_file, q.mypayload, 1388);
    q.index = i;

    // Checksum for corrupt receive
    q.checksum = q.index;
    q.checksum ^= q.len;
    int j;
    for (j = 0; j < 1388; j++)
    	q.checksum ^= q.mypayload[j];
    
    // Adding message in vector
    mymsg_vec[i] = q;

    memcpy(t.payload, &q, 1400);
    send_message(&t);
  }

  // Empty the sliding window
  for (i = 0; i < window_size; i++) {
  	// Checking every message by index
    if (recv_message_timeout(&r, delay) >= 0) {
    	// In ack we have the index of this message
      	sended[atoi(r.payload)] = 1;
    }
  }

  // Resend unreceived messages
  for (i = 0; i < cadre; i++) {
  	// "Force sending" until received
    while (sended[i] == 0) {
      memcpy(t.payload, &mymsg_vec[i], 1400);
      send_message(&t);
      if (recv_message_timeout(&r, delay) > 0) {
        sended[i] = 1;
      }
    }
  }

// Free memory
free(sended);
free(mymsg_vec);
close(input_file);

return 0;
}
