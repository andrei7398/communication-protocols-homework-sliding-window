Sfetcu Iulian-Andrei - 322CD - Tema 1 PC

Structura mea (mymsg) :
	- int checksum (folosit pentru verificare la receive);
	- int index (folosit pentru stocarea mesajelor in ordine la
	receive, intr-un vector de mesaje);
	- int len (lungimea mesajului trimis);
	- char[1388] mypayload (un buffer de 1388 de caractere in care
	pun continutul citit din fisier)

----------------------------- send.c ------------------------------

 1. Citesc numele fisierului si il trimit intr-un while pana e primit.
 2. Calculez dimensiunea fisierului si o trimit intr-un while pana e primita.
 3. Calculez numarul de cadre si dimensiunea ferestrei ca in laborator.
 4. Aloc un vector de mesaje in care tin toate mesajele trimise in 
 eventualitatea retrimiterii (il eliberez la final).
 5. Aloc un vector de evidenta pentru mesajele trimise cu succes sau fail 
 (1)/(0).
 6. Umplu fereastra.
 7. O mentin plina pana cand termin numarul de cadre. In eventualitatea unui
 mesaj receptionat ca fiind OK, il marchez in vectorul de evidenta.
 8. Golesc fereastra repetand acelasi procedeu.
 9. Iterez prin vectorul de evidenta si fortez trimiterea oricarui mesaj 
 pierdut/corupt (stop & wait).
 10. Eliberez memoria.


----------------------------- recv.c ------------------------------

 1. Primesc numele.
 2. Primesc lungimea fisierului recursiv (in caz de loss/corupere).
 3. Primesc cate un mesaj si verific daca e corupt/nu.
 4. In caz pozitiv nu returnez ack (o sa rezulte un resend din partea)
 send-ului.
 5. In caz ca nu e corupt, incrementez counterul (stiu ca trebuie sa 
 primesc un anumit numar de cadre) si adaug mesajul in vectorul de mesaje.
 6. Daca am primit toate mesajele, scriu in fisier iterand prin vector.
 7. Eliberez memoria.

OBS: Mentionez ca primesc 38p pe checkerul local. Thank God.